/**
 * @module Application
 */
"use strict";

var config = require('./config');
var mongo = require("mongodb");
var mongodbUri = config.get('mongo.uri');
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({ port: config.get('app.port') });

var buffer = [];
var connect = false;

mongo.MongoClient.connect (mongodbUri, function (err, db) {

    /*
     * инициализация данных
     */
    db.collection('tasks', function(err, tasks) {
        tasks.find({status:-1}).toArray(function(err, arTasks) {
            arTasks.forEach(function (task,i) {
                buffer.push(task);
            });
        });
    });

    /*
     * работа с коннектами от клиента
     */
    wss.on('connection', function connection(ws) {
        connect = ws;
        // при наличие коннекта и не пустого кеша задачь передаем их клиенту на обработку
        if (buffer.length > 0) {
            buffer.forEach(function (item, i, arr) {
                ws.send(JSON.stringify(item));
            })
        }
        ws.on('message', function incoming(msg) {
            // обрабатываем ответы клиента
            var data = JSON.parse(msg);
            data.status = 0;
            var tasks = db.collection('tasks');
            var key_id = new mongo.ObjectID(data.key);
            tasks.update({key: key_id}, { $set: {status:0} }, function(err, result) {
                });
        });
        ws.on('close', function () {
            connect = false;
        });
        ws.on('error', function () {
            connect = false;
        });
    });

    /*
     * имитация очереди задач
     */
    db.collection('messages', function(err, collection) {
        // открывааем tailable cursors для capped коллекций  - курсор остается открытым и передает новые данные
        collection.find({}, {tailable:true, awaitdata:true, numberOfRetries:-1}).sort({ $natural: 1 }).each(function(err, doc) {
            // таблица выполняет роль очереди
            db.collection('tasks', function(err, tasks) {
                var data = {
                    key: doc._id,
                    status:-1,
                    cmd: 'ls'
                };
                // добавляем новые данные в очередь
                tasks.update({key: doc._id}, {$setOnInsert: data}, {upsert: true}, function(err, task, res) {
                    if (res.upserted) {
                        buffer.push(data);
                        // если в момент поступления данных есть открытый коннект передаем в него вновь созданый пакет данных
                        if (connect) {
                            connect.send(JSON.stringify(data));
                        }
                    }
                });
            });
        });
    });
});