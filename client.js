var config = require('./config');
var WebSocket = require('ws');
var ws = new WebSocket('ws://localhost:'+config.get('app.port'));
 
ws.on('open', function open() {
});
 
ws.on('message', function(msg, flags) {
	var data = JSON.parse(msg);
	data.report = "success";
	ws.send(JSON.stringify(data));
});
