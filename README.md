# Тестовое задание

Перед началом необходимо создать capped коллекцию

`` use test ``  
`` db.createCollection('messages', { capped: true, size: 100000 }); ``

Старт приложения
`` npm start``

Добавление записи
`` db.messages.insert({});``

Старт клиента
`` node client ``
